#####################################################
#
#   Name: Alexandros Mittos
#   Institute: University College London
#   Date: March 2018
#   Language: Python 3
#
#   Information: Textblob examples
#
#
#
#
#####################################################

#############################
#        Imports            #
#############################

import datetime
from textblob import TextBlob

#############################
#        Variables          #
#############################

text = '''
The titular threat of The Blob has always struck me as the ultimate movie
monster: an insatiably hungry, amoeba-like mass able to penetrate
virtually any safeguard, capable of--as a doomed doctor chillingly
describes it--"assimilating flesh on contact.
Snide comparisons to gelatin be damned, it's a concept with the most
devastating of potential consequences, not unlike the grey goo scenario
proposed by technological theorists fearful of
artificial intelligence run rampant.
'''

#############################
#        Functions          #
#############################

# Awesome function No.1
def super_function():

    blob = TextBlob(text)

    print(blob.tags)
    print(blob.noun_phrases)
    print(blob.words)
    # print(blob.sentences)

    print(blob.ngrams(n=3))

    for sentence in blob.sentences:
        print(sentence.sentiment)



#############################
#           Main            #
#############################

if __name__ == "__main__":

    print('Process started...\n')

    # Start timer
    start = datetime.datetime.now()

    # Run awesome code
    super_function()

    # End timer
    end = datetime.datetime.now()

    # Print results
    print("\nProcess finished")
    print("Total time: " + str(end - start))

#############################
#       End of Script       #
#############################