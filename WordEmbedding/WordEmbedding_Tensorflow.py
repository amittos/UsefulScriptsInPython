#####################################################
#
#   Name: Alexandros Mittos
#   Institute: University College London
#   Date: March 2018
#   Language: Python 3
#
#   Information: Word Embedding Example using Tensorflow
#   Source 1: http://adventuresinmachinelearning.com/word2vec-tutorial-tensorflow/
#   Source 2: https://github.com/adventuresinML/adventures-in-ml-code
#
#   Notes: Tensorflow needs to use Python 64bit
#
#####################################################

#############################
#        Imports            #
#############################

import datetime
import urllib.request
import collections
import math
import os
import random
import zipfile
import datetime as dt
import numpy as np
import tensorflow as tf

#############################
#        Variables          #
#############################

data_index = 0

#############################
#        Functions          #
#############################

# Automatically download needed files
def maybe_download(filename, url, expected_bytes):

    if not os.path.exists(filename):
        filename, _ = urllib.request.urlretrieve(url + filename, filename)
    statinfo = os.stat(filename)

    if statinfo.st_size == expected_bytes:
        print('Found and verified', filename)
    else:
        print(statinfo.st_size)
        raise Exception(
            'Failed to verify ' + filename + '. Can you get to it with a browser?')

    return filename


# Read the data into a list of strings.
def read_data(filename):

    # Extract the first file enclosed in a zip file as a list of words.
    with zipfile.ZipFile(filename) as f:
        data = tf.compat.as_str(f.read(f.namelist()[0])).split()

    return data


# Process raw inputs into a dataset.
def build_dataset(words, n_words):

    # Create a “counter” list, which will store the number of times a word is found within the data-set.
    # Because we are restricting our vocabulary to only 10,000 words, any words not within the top 10,000
    # most common words will be marked with an “UNK” designation, standing for “unknown”.
    # The initialized count list is then extended, using the Python collections module and the Counter() class
    # and the associated most_common() function.  These count the number of words in the given argument (words)
    # and then returns the n most common words in a list format.

    count = [['UNK', -1]]
    count.extend(collections.Counter(words).most_common(n_words - 1))

    # The next part of this function creates a dictionary, called dictionary which is populated by keys
    # corresponding to each unique word.  The value assigned to each unique word key is simply an increasing
    # integer count of the size of the dictionary.  So, for instance, the most common word will receive the
    # value 1, the second most common the value 2, the third most common word the value 3, and so on (the
    # integer 0 is assigned to the ‘UNK’ words). This step creates a unique integer value for each word within
    # the vocabulary – accomplishing the second step of the process which was defined above.

    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)
    data = list()

    # Next, the function loops through each word in our full words data set – the data set which was output
    # from the read_data() function.  A list called data is created, which will be the same length as words
    # but instead of being a list of individual words, it will instead be a list of integers – with each word
    # now being represented by the unique integer that was assigned to this word in dictionary.  So, for the
    # first sentence of our data-set [‘anarchism’, ‘originated’, ‘as’, ‘a’, ‘term’, ‘of’, ‘abuse’], now looks
    # like this in the data variable: [5242, 3083, 12, 6, 195, 2, 3136].  This part of the function addresses
    # step 3 in the list above.

    unk_count = 0
    for word in words:
        if word in dictionary:
            index = dictionary[word]
        else:
            index = 0  # dictionary['UNK']
            unk_count += 1
        data.append(index)
    count[0][1] = unk_count

    # Finally, the function creates a dictionary called reverse_dictionary that allows us to look up a word
    # based on its unique integer identifier, rather than looking up the identifier based on the word i.e. the
    # original dictionary.

    reversed_dictionary = dict(zip(dictionary.values(), dictionary.keys()))

    return data, count, dictionary, reversed_dictionary


def collect_data(vocabulary_size = 10000):

    url = 'http://mattmahoney.net/dc/'
    filename = maybe_download('text8.zip', url, 31344016)
    vocabulary = read_data(filename)
    print(vocabulary[:7])
    data, count, dictionary, reverse_dictionary = build_dataset(vocabulary, vocabulary_size)
    del vocabulary  # Hint to reduce memory.
    return data, count, dictionary, reverse_dictionary


# Generate batch data
def generate_batch(data, batch_size, num_skips, skip_window):

    # This function will generate mini-batches to use during our training. These batches will consist of
    # input words (stored in batch) and random associated context words within the gram as the labels to
    # predict (stored in context).  For instance, in the 5-gram “the cat sat on the”, the input word will
    # be center word i.e. “sat” and the context words that will be predicted will be drawn randomly from
    # the remaining words of the gram: [‘the’, ‘cat’, ‘on’, ‘the’].  In this function, the number of words
    # drawn randomly from the surrounding context is defined by the argument num_skips. The size of the
    # window of context words to draw from around the input word is defined in the argument skip_window –
    # in the example above (“the cat sat on the”), we have a skip window width of 2 around the input word “sat”.

    global data_index
    assert batch_size % num_skips == 0
    assert num_skips <= 2 * skip_window
    batch = np.ndarray(shape=(batch_size), dtype=np.int32)
    context = np.ndarray(shape=(batch_size, 1), dtype=np.int32)
    span = 2 * skip_window + 1  # [ skip_window input_word skip_window ]

    # Below, first the batch and label outputs are defined as variables of size batch_size.
    # Then the span size is defined, which is basically the size of the word list that the input
    # word and context samples will be drawn from. In the example sub-sentence above “the cat sat on the”,
    # the span is 5 = 2 x skip window + 1.

    buffer = collections.deque(maxlen=span)
    for _ in range(span):
        buffer.append(data[data_index])
        data_index = (data_index + 1) % len(data)


    # The code below fills out the batch and context variables. The first “target” word selected is the word
    # at the center of the span of words and is therefore the input word.  Then other words are randomly
    # selected from the span of words, making sure that the input word is not selected as part of the context,
    # and each context word is unique. The batch variable will feature repeated input words (buffer[skip_window])
    # which are matched with each context word in context.

    for i in range(batch_size // num_skips):
        target = skip_window  # input word at the center of the buffer
        targets_to_avoid = [skip_window]
        for j in range(num_skips):
            while target in targets_to_avoid:
                target = random.randint(0, span - 1)
            targets_to_avoid.append(target)
            batch[i * num_skips + j] = buffer[skip_window]  # this is the input word
            context[i * num_skips + j, 0] = buffer[target]  # these are the context words
        buffer.append(data[data_index])
        data_index = (data_index + 1) % len(data)
    # Backtrack a little bit to avoid skipping words in the end of a batch
    data_index = (data_index + len(data) - span) % len(data)

    # The batch and context variables are then returned – and now we have a means of drawing batches of
    # data from the data set.  We are now in a position to create our Word2Vec training code in TensorFlow.

    return batch, context

def run(graph, num_steps):
    with tf.Session(graph=graph) as session:
      # We must initialize all variables before we use them.
      init.run()
      print('Initialized')

      average_loss = 0
      for step in range(num_steps):
        batch_inputs, batch_context = generate_batch(data,
            batch_size, num_skips, skip_window)
        feed_dict = {train_inputs: batch_inputs, train_context: batch_context}

        # We perform one update step by evaluating the optimizer op (including it
        # in the list of returned values for session.run()
        _, loss_val = session.run([optimizer, cross_entropy], feed_dict=feed_dict)
        average_loss += loss_val

        if step % 2000 == 0:
          if step > 0:
            average_loss /= 2000
          # The average loss is an estimate of the loss over the last 2000 batches.
          print('Average loss at step ', step, ': ', average_loss)
          average_loss = 0

        # Note that this is expensive (~20% slowdown if computed every 500 steps)
        if step % 10000 == 0:
          sim = similarity.eval()
          for i in range(valid_size):
            valid_word = reverse_dictionary[valid_examples[i]]
            top_k = 8  # number of nearest neighbors
            nearest = (-sim[i, :]).argsort()[1:top_k + 1]
            log_str = 'Nearest to %s:' % valid_word
            for k in range(top_k):
              close_word = reverse_dictionary[nearest[k]]
              log_str = '%s %s,' % (log_str, close_word)
            print(log_str)
      final_embeddings = normalized_embeddings.eval()



#############################
#           Main            #
#############################

if __name__ == "__main__":

    print('Process started...\n')

    # Start timer
    start = datetime.datetime.now()

    # Set the size of the vocabulary. This means that we will check the 10,000 most unique words.
    vocabulary_size = 10000
    # Get the data, dictionary, etc.
    data, count, dictionary, reverse_dictionary = collect_data(vocabulary_size=vocabulary_size)

    # We pick a random validation set to sample nearest neighbors. Here we limit the
    # validation samples to the words that have a low numeric ID, which by
    # construction are also the most frequent.
    valid_size = 16  # Random set of words to evaluate similarity on.
    valid_window = 100  # Only pick dev samples in the head of the distribution.
    valid_examples = np.random.choice(valid_window, valid_size, replace=False)
    num_sampled = 64  # Number of negative examples to sample.

    # Below I will step through the process of creating our Word2Vec word embeddings in TensorFlow.
    # What does this involve?  Simply, we need to setup the neural network which I previously presented,
    # with a word embedding matrix acting as the hidden layer and an output softmax layer in TensorFlow.
    # By training this model, we’ll be learning the best word embedding matrix and therefore we’ll be
    # learning a reduced, context maintaining, mapping of words to vectors.

    batch_size = 128
    embedding_size = 300  # Dimension of the embedding vector.
    skip_window = 2  # How many words to consider left and right.
    num_skips = 2  # How many times to reuse an input to generate a label.

    # Tensorflow Part
    graph = tf.Graph()

    with graph.as_default():

        # Input data.
        # We setup some TensorFlow placeholders that will hold our input words (their integer indexes) and
        # context words which we are trying to predict. We also need to create a constant to hold our
        # validation set indexes in TensorFlow.

        train_inputs = tf.placeholder(tf.int32, shape=[batch_size])
        train_context = tf.placeholder(tf.int32, shape=[batch_size, 1])
        valid_dataset = tf.constant(valid_examples, dtype=tf.int32)

        # Look up embeddings for inputs.
        # We need to setup the embedding matrix variable / tensor – this is straight-forward using the
        # TensorFlow embedding_lookup() function. The first step in the code below is to create the embeddings
        # variable, which is effectively the weights of the connections to the linear hidden layer. We initialize
        # the variable with a random uniform distribution between -1.0 to 1.0. The size of this variable is
        # (vocabulary_size, embedding_size) – the vocabulary_size is the 10,000 words that we have used to setup
        # our data in the previous section. This is basically our one-hot vector input, where the only element
        # with a value of “1” is the current input word, all the other values are set to “0”. The second
        # dimension, embedding_size, is our hidden layer size, and is the length of our new, smaller,
        # representation of our words.  We can also think of this tensor as a big lookup table – the rows are
        # each word in our vocabulary, and the columns are our new vector representation of each of these words.
        # Here’s a simplified example (using dummy values), where vocabulary_size=7 and embedding_size=3

        # The next line in the code involves the tf.nn.embedding_lookup() function, which is a useful helper
        # function in TensorFlow for this type of task. Here’s how it works – it takes an input vector of
        # integer indexes – in this case our train_input tensor of training input words, and “looks up” these
        # indexes in the supplied embeddings tensor. Therefore, this command will return the current embedding
        # vector for each of the supplied input words in the training batch. The full embedding tensor will be
        # optimized during the training process.

        embeddings = tf.Variable(
            tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0))
        embed = tf.nn.embedding_lookup(embeddings, train_inputs)

        # The weight variable, as it is connecting the hidden layer and the output layer, is of size
        # (out_layer_size, hidden_layer_size) = (vocabulary_size, embedding_size). The biases, as usual, will
        # only be single dimensional and the size of the output layer. We then multiply the embedded variable
        # (embed) by the weights and add the bias. Now we are ready to create a softmax operation and we will
        # use cross entropy loss to optimize the weights, biases and embeddings of the model. To do this easily,
        # we will use the TensorFlow function softmax_cross_entropy_with_logits(). However, to use this function
        # we first have to convert the context words / integer indices into one-hot vectors.

        weights = tf.Variable(
            tf.truncated_normal([embedding_size, vocabulary_size],
                                stddev=1.0 / math.sqrt(embedding_size)))
        biases = tf.Variable(tf.zeros([vocabulary_size]))
        hidden_out = tf.transpose(tf.matmul(tf.transpose(weights), tf.transpose(embed))) + biases

        # Convert train_context to a one-hot format
        train_one_hot = tf.one_hot(train_context, vocabulary_size)

        cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=hidden_out, labels=train_one_hot))

        # Construct the SGD optimizer using a learning rate of 1.0.
        optimizer = tf.train.GradientDescentOptimizer(1.0).minimize(cross_entropy)

        # Compute the cosine similarity between minibatch examples and all embeddings.
        norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keep_dims=True))
        normalized_embeddings = embeddings / norm
        valid_embeddings = tf.nn.embedding_lookup(
            normalized_embeddings, valid_dataset)


        similarity = tf.matmul(valid_embeddings, normalized_embeddings, transpose_b=True)

        # Add variable initializer.
        init = tf.global_variables_initializer()

    num_steps = 100
    softmax_start_time = dt.datetime.now()
    run(graph, num_steps=num_steps)
    softmax_end_time = dt.datetime.now()
    print("Softmax method took {} minutes to run 100 iterations".format((softmax_end_time - softmax_start_time).total_seconds()))

    with graph.as_default():

        # Construct the variables for the NCE loss
        # Instead of taking the probability of the context word compared to all of the possible context words
        # in the vocabulary, this method randomly samples 2-20 possible context words and evaluates the
        # probability only from these. I won’t go into the nitty gritty details here, but suffice to say that
        # this method has been shown to perform well and drastically speeds up the training process.

        # TensorFlow has helped us out here, and has supplied an NCE loss function that we can use called
        # tf.nn.nce_loss() which we can supply weight and bias variables to. Using this function, the time
        # to perform 100 training iterations reduced from 25 seconds with the softmax method to less than
        # 1 second using the NCE method.

        nce_weights = tf.Variable(tf.truncated_normal([vocabulary_size, embedding_size],
                                    stddev=1.0 / math.sqrt(embedding_size)))
        nce_biases = tf.Variable(tf.zeros([vocabulary_size]))

        nce_loss = tf.reduce_mean(tf.nn.nce_loss(weights=nce_weights,
                                    biases=nce_biases,
                                    labels=train_context,
                                    inputs=embed,
                                    num_sampled=num_sampled,
                                    num_classes=vocabulary_size))

        optimizer = tf.train.GradientDescentOptimizer(1.0).minimize(nce_loss)

        # Add variable initializer.
        init = tf.global_variables_initializer()

    # Now we are good to run the code. As discussed, every 10,000 iterations the code outputs the validation
    # words and the words that the Word2Vec system deems are similar.

    num_steps = 50000
    nce_start_time = dt.datetime.now()
    run(graph, num_steps)
    nce_end_time = dt.datetime.now()
    print("NCE method took {} minutes to run 100 iterations".format((nce_end_time - nce_start_time).total_seconds()))

    # In summary then, we have learnt how to use the Word2Vec methodology to reduce large one-hot word vectors
    # to much reduced word embedding vectors which preserve the context and meaning of the original words. These
    # word embedding vectors can then be used as a more efficient and effective input to deep learning techniques
    # which aim to model natural language.

    # End timer
    end = datetime.datetime.now()

    # Print results
    print("\nProcess finished")
    print("Total time: " + str(end - start))

#############################
#       End of Script       #
#############################