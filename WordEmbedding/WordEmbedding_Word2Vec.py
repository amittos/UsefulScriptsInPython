#####################################################
#
#   Name: Alexandros Mittos
#   Institute: University College London
#   Date: March 2018
#   Language: Python
#
#   Information: Word Embedding Examples
#   Source: https://machinelearningmastery.com/develop-word-embeddings-python-gensim/
#
#####################################################

#############################
#        Imports            #
#############################

import datetime
from gensim.models import Word2Vec
from sklearn.decomposition import PCA
from matplotlib import pyplot
from gensim.models import KeyedVectors

#############################
#        Functions          #
#############################

# Word Embedding using Word2Vec
def Word2Vec_Embedding():

    # Define training data using these 5 simple sentences.
    sentences = [['this', 'is', 'the', 'first', 'sentence', 'for', 'word2vec'],
                 ['this', 'is', 'the', 'second', 'sentence'],
                 ['yet', 'another', 'sentence'],
                 ['one', 'more', 'sentence'],
                 ['and', 'the', 'final', 'sentence']]

    # Train the model using Word2Vec.
    model = Word2Vec(sentences, min_count=1)

    # Print a summary of the model
    print(model)

    # Print a summary of the vocabulary
    words = list(model.wv.vocab)
    print(words)

    # Access vector for one word
    print(model['sentence'])

    # Save model
    model.save('model.bin')

    # Load model
    new_model = Word2Vec.load('model.bin')

    print(new_model)

# Word Embedding using Word2Vec -- Scatterplot example
def PlotVectors():

    # Define training data using these 5 simple sentences.
    sentences = [['this', 'is', 'the', 'first', 'sentence', 'for', 'word2vec'],
                 ['this', 'is', 'the', 'second', 'sentence'],
                 ['yet', 'another', 'sentence'],
                 ['one', 'more', 'sentence'],
                 ['and', 'the', 'final', 'sentence']]

    # Train the model using Word2Vec.
    model = Word2Vec(sentences, min_count=1)

    # Fit a 2d PCA model to the vectors
    X = model[model.wv.vocab]
    pca = PCA(n_components=2)
    result = pca.fit_transform(X)

    # Create a scatter plot of the projection
    pyplot.scatter(result[:, 0], result[:, 1])
    words = list(model.wv.vocab)
    for i, word in enumerate(words):
        pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
    pyplot.show()

# How to use Google's pre-trained model
def Word2Vec_Google():

    # Load the google word2vec model
    # Download from here: https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM (3GB+)
    filename = 'GoogleNews-vectors-negative300.bin'

    # Train the model based on the file
    model = KeyedVectors.load_word2vec_format(filename, binary=True)

    # Calculate: (king - man) + woman = ?
    result = model.most_similar(positive=['woman', 'king'], negative=['man'], topn=1)

    print(result)
    # It prints: [('queen', 0.7118192315101624)]

# Use Stanford's pre-trained model (GloVe)
def Word2Vec_Stanford():

    # Load the Stanford GloVe model
    # Download from here: https://nlp.stanford.edu/projects/glove/
    filename = 'glove.6B.100d.txt.word2vec'

    # Train the model
    model = KeyedVectors.load_word2vec_format(filename, binary=False)

    # Calculate: (king - man) + woman = ?
    result = model.most_similar(positive=['woman', 'king'], negative=['man'], topn=1)

    print(result)
    # [('queen', 0.7698540687561035)]


#############################
#           Main            #
#############################

if __name__ == "__main__":

    print('Process started...\n')

    # Start timer
    start = datetime.datetime.now()

    # Run Word2Vec Embedding example
    Word2Vec_Embedding()

    # Run Word2Vec Embedding scatterplot example
    PlotVectors()

    # Use Google's pre-trained model
    # Word2Vec_Google()

    # Use Stanford's pre-trained model
    # Word2Vec_Stanford()

    # End timer
    end = datetime.datetime.now()

    # Print results
    print("\nProcess finished")
    print("Total time: " + str(end - start))

#############################
#       End of Script       #
#############################