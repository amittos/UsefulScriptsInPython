This repository contains several ready-to-use scripts for future use. 

Some of these scripts are obtained from StackOverflow answers, others from Coursera courses, and some of them I have written from scratch my self. 

Each folder represents a certain Python library. 

Enjoy. 