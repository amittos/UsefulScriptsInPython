#####################################################
#                                                   #
#   Alexandros Mittos                               #
#                                                   #
#   This script checks whether the user accounts    #
#   of the keyword-based dataset are bots or not    #
#   using Botometer (botometer.iuni.iu.edu/)        #
#                                                   #
#####################################################

#############################
#        Imports            #
#############################

import datetime
import botometer
import time
import requests
import tweepy

#############################
#        Variables          #
#############################

mashape_key = ""
twitter_app_auth = {
    'consumer_key': '',
    'consumer_secret': '',
    'access_token': '',
    'access_token_secret': '',
}

# Store the auth into the variable
bom = botometer.Botometer(mashape_key=mashape_key, **twitter_app_auth)

#############################
#        Functions          #
#############################

# Awesome function No.1
def super_function(filename):

    usernames = ['alex']

    # Classify the accounts
    # This part takes the longest to complete, ~15 minutes per 100 usernames
    # The total output will be written AFTER the evaluation is complete
    print('Starting the evaluation of the accounts now. This will take a while...')
    for item in usernames:
        try:
            # Start timer
            start = datetime.datetime.now()
            print(item)

            # Create the classification object
            classification = bom.check_account(item)

            # Save the contents into variables
            content = classification['categories']['content']
            friend = classification['categories']['friend']
            network = classification['categories']['network']
            sentiment = classification['categories']['sentiment']
            temporal = classification['categories']['temporal']
            user = classification['categories']['user']
            score_en = classification['scores']['english']
            score_un = classification['scores']['universal']
            screen_name = classification['user']['screen_name']

            print(  str(screen_name) + '\t' + str(content) + '\t' + str(friend) + '\t' +
                    str(network) + '\t' + str(sentiment) + '\t' + str(temporal) + '\t' +
                    str(user) + '\t' + str(score_en) + '\t' + str(score_un))

            # End timer
            end = datetime.datetime.now()

            # Print results
            print("\nProcess finished")
            print("Total time: " + str(end - start))

        except tweepy.error.TweepError as e:
            print(e)
        except botometer.NoTimelineError:
            print('ERROR: Account ' + item + ' does\'t have any tweets.')
        except TimeoutError:
            print('TIMEOUT ERROR')
            time.sleep(600)
        except requests.exceptions.HTTPError:
            print('requests.exceptions.HTTPError: 502 Server Error: Bad Gateway')
        except requests.exceptions.ConnectionError as e:
            print(e)
        except requests.packages.urllib3.exceptions.MaxRetryError as e:
            print(e)
        except requests.packages.urllib3.exceptions.NewConnectionError as e:
            print(e)

    # Inform the user because he's stupid...
    print('Results written.')

#############################
#           Main            #
#############################

if __name__ == "__main__":

    print('Process started...\n')

    # Start timer
    start = datetime.datetime.now()

    super_function()

    # End timer
    end = datetime.datetime.now()

    # Print results
    print("\nProcess finished")
    print("Total time: " + str(end - start))

#############################
#       End of Script       #
#############################