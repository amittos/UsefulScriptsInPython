from __future__ import print_function

#####################################################
#
#   Name: Alexandros Mittos
#   Language: Python
#   Version: 2
#
#   Conducts a URL characterization using
#   the Web Shrinker API
#   https://dashboard.webshrinker.com/
#
#####################################################

#############################
#        Imports            #
#############################

import time
import datetime
import requests
from base64 import urlsafe_b64encode

#############################
#      Variables/Paths      #
#############################

##################################################
# This for retarded Python 2, because it does not
# recognize utf-8 otherwise
##################################################

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

###################################################

# Web Shrinker keys
key = ""
secret_key = ""

#############################
#        Functions          #
#############################

# Awesome function No.1
def url_analysis():

    urls = ['www.google.com']

    for url in urls:
        try:

            # Get the list of the urls
            target_website = url[:-1]
            api_url = "https://api.webshrinker.com/categories/v2/%s" % urlsafe_b64encode(target_website)

            # Classic shit
            response = requests.get(api_url, auth=(key, secret_key))
            status_code = response.status_code
            data = response.json()

            # If status is OK
            if status_code == 200:
                # Do something with the JSON response
                categories = data['data'][0]['categories']
                print("%s: %s" % (target_website, ", ".join(categories)))
            elif status_code == 202:
                # The request is being categorized right now in real time, check again for an updated answer
                print("The categories for '%s' are being determined, check again in a few seconds" % target_website)
            else:
                # The different status codes are covered in the documentation (http://docs.webshrinker.com/v2/website-category-api.html#category-lookup)
                print("An error occurred: HTTP %d" % status_code)
                if 'error' in data:
                    print(data['error']['message'])
        except Exception as e:
            print(e)

#############################
#           Main            #
#############################

if __name__ == "__main__":

    print('Process started...\n')

    # Start timer
    start = datetime.datetime.now()

    url_analysis()

    # End timer
    end = datetime.datetime.now()

    # Print results
    print("\nProcess finished")
    print("Total time: " + str(end - start))

#############################
#       End of Script       #
#############################