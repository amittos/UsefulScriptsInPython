#####################################################
#
#   Alexandros Mittos
#   Language: Python 
#   Version: 3
#
#   A script to crawl results made by Site Advisor
#   (currently not working)
#
#####################################################

#############################
#        Imports            #
#############################

import datetime
import re
import requests
import urllib.parse

#############################
#        Functions          #
#############################

# Count number of occurrences in a list
def count_occur(list):
    return [[x, list.count(x)] for x in set(list)]

# Extract the URL from a file and characterize them
def super_function():

    categories = []
    mal = []
    exceptions = 0
    list_of_URLs = []
    url_counter = 0

    # A mockup list to test the function
    list_of_URLs = ['www.google.com', 'www.bing.com', 'www.bbc.co.uk', 'www.ucl.ac.uk']

    # Start the URL analysis
    for item in list_of_URLs:

        category = None
        maliciousness = None

        # Parse the URL
        url = str(item)
        try:
            final_url = requests.get(url, timeout=60).url
        except:
            final_url = url
        domain = urllib.parse.urlparse(final_url).netloc

        # Use siteadvisor to put URL in a category
        if domain not in categories:

            # time.sleep(1)

            try:
                mcafee = requests.get("http://www.siteadvisor.com/sites/%s" % url, timeout=60)
            except 'Timeout':
                print("Timeout, continuing to the next...")
                exceptions += 1
                continue

            try:
                category = mcafee.text.split("Website Category")[1].split(">")[4].split("<")[0]
                try:
                    maliciousness = mcafee.text.split("This link is ")[1].split(".")[0]
                except 'Unknown':
                    exceptions += 1
                    maliciousness = "Unknown"
            except 'Error':
                print("Error, continuing to the next...")
                exceptions += 1
                continue

            print(category)
            categories.append(category)
            mal.append(maliciousness)


    print([[x, categories.count(x)] for x in set(categories)])

    # Print the results
    # print('Categories: ' + str(count_occur(categories))) # Counter method
    # print('Maliciousness: ' + str(count_occur(mal))) # Counter method

    if exceptions > 0:
        print('Number of exceptions: ' + str(exceptions))

    results.write("\n" + filename + ": \n" + str(url_counter) + " URLs found\n\n")
    results.write('Categories: ' + str(count_occur(categories)) + "\n")
    results.write('Maliciousness: ' + str(count_occur(mal)) + "\n")

    results.close()

#############################
#           Main            #
#############################

if __name__ == "__main__":

    print('Process started...\n')

    # Start timer
    start = datetime.datetime.now()

    # Run awesome code
    super_function()

    # End timer
    end = datetime.datetime.now()

    # Print results
    print("\nProcess finished")
    print("Total time: " + str(end - start))

#############################
#       End of Script       #
#############################