#####################################################
#
#   Alexandros Mittos
#   Language: Python 
#   Version: 3
#
#   A simple script to conduct Jaccard on 
#   wordlists instead of numbers
#
#####################################################

#############################
#        Imports            #
#############################

import datetime
import numpy as np
from sklearn.metrics import jaccard_similarity_score

#############################
#        Variables          #
#############################

#############################
#        Functions          #
#############################

def jaccard_distance(list1, list2):
    intersection = len(list(set(list1).intersection(list2)))
    print(list(set(list1).intersection(list2)))
    union = (len(list1) + len(list2)) - intersection
    return float(intersection / union)

# Awesome function No.1
def super_function():

    print(jaccard_distance(users_PrecisionMedicine, users_PersonalizedMedicine))

#############################
#           Main            #
#############################

if __name__ == "__main__":

    print('Process started...\n')

    # Start timer
    start = datetime.datetime.now()

    super_function()

    # End timer
    end = datetime.datetime.now()

    # Print results
    print("\nProcess finished")
    print("Total time: " + str(end - start))

#############################
#       End of Script       #
#############################