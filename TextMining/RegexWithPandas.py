#####################################################
#
#   Name: Alexandros Mittos
#   Institute: University College London
#   Date: March 2018
#   Language: Python
#
#   Information: How to use Regex with Pandas
#
#####################################################

#############################
#        Imports            #
#############################

import re
import datetime
import pandas as pd

#############################
#        Variables          #
#############################

text1 = '@UN @UN_Women "Ethics are built right into the ideals and objectives of the United Nations" \
#UNSG @ NY Society for Ethical Culture bit.ly/2guVelr'

# Input Text
time_sentences = ["Monday: The doctor's appointment is at 2:45pm.",
                  "Tuesday: The dentist's appointment is at 11:30 am.",
                  "Wednesday: At 7:00pm, there is a basketball game!",
                  "Thursday: Be back home by 11:15 pm at the latest.",
                  "Friday: Take the train at 08:10 am, arrive at 09:00am."]

#############################
#        Functions          #
#############################

def re_examples():

    text2 = text1.split(' ')

    # We can use regular expressions to help us with more complex parsing.
    #
    # For example '@[A-Za-z0-9_]+' will return all words that:
    #
    # start with '@' and are followed by at least one:
    # capital letter ('A-Z')
    # lowercase letter ('a-z')
    # number ('0-9')
    # or underscore ('_')

    text = [w for w in text2 if re.search('@[A-Za-z0-9_]+', w)]
    print(text)

def pandas_examples():

    # Create a DataFrame using the input text
    df = pd.DataFrame(time_sentences, columns=['text'])
    print(df)
    print('___')

    # Find the number of characters for each string in df['text']
    print(df['text'].str.len())
    print('___')

    # Find the number of *tokens* for each string in df['text']
    # By default it uses space to split the sentence
    print(df['text'].str.split().str.len())
    print('___')

    # Find which entries contain the word 'appointment'
    print(df['text'].str.contains('appointment'))
    print('___')

    # Find how many times a *digit* occurs in each string
    print(df['text'].str.count(r'\d'))
    print('___')

    # Find all digits
    print(df['text'].str.findall(r'\d'))
    print('___')

    # Group and find the hours and minutes
    print(df['text'].str.findall(r'(\d?\d):(\d\d)'))
    print('___')

    # Replace weekdays with '???'
    print(df['text'].str.replace(r'\w+day\b', '???'))
    print('___')

    # Replace weekdays with 3 letter abbreviations
    print(df['text'].str.replace(r'(\w+day\b)', lambda x: x.groups()[0][:3]))
    print('___')

    # Create new columns from first match of extracted groups
    print(df['text'].str.extract(r'(\d?\d):(\d\d)'))
    print('___')

    # Extract the entire time, the hours, the minutes, and the period
    print(df['text'].str.extractall(r'((\d?\d):(\d\d) ?([ap]m))'))
    print('___')

    # Extract the entire time, the hours, the minutes, and the period with group names
    print(df['text'].str.extractall(r'(?P<time>(?P<hour>\d?\d):(?P<minute>\d\d) ?(?P<period>[ap]m))'))
    print('___')

#############################
#           Main            #
#############################

if __name__ == "__main__":

    print('Process started...\n')

    # Start timer
    start = datetime.datetime.now()

    re_examples()
    pandas_examples()

    # End timer
    end = datetime.datetime.now()

    # Print results
    print("\nProcess finished")
    print("Total time: " + str(end - start))

#############################
#       End of Script       #
#############################