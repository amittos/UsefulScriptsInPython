#########################################################
#
#   Alexandros Mittos
#
#   LDA Analysis Example
#
#########################################################

import nltk
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from gensim import corpora, models
from collections import defaultdict
from string import digits
import langid
import datetime
import re

results = open('lda_results.txt', 'a')

# List of stop words
stoplist_tw = ['amp', 'get', 'got', 'hey', 'hmm', 'hoo', 'hop', 'iep', 'let', 'ooo', 'par']


def filter_lang(lang, documents):
    doclang = [langid.classify(doc) for doc in documents]
    return [documents[k] for k in range(len(documents)) if doclang[k][0] == lang]


def lda_analysis(txt_file, keyword, number_of_topics, passes):

    print("LDA model running for " + keyword + ", please wait...")

    # Make a list with all the tweets
    tweet_list = open(input_path + txt_file, encoding='utf-8', errors='ignore').readlines()

    #  Filter NON english documents
    documents = filter_lang('en', tweet_list)
    print("We have " + str(len(documents)) + " documents in english ")

    # Remove urls
    documents = [re.sub(r"(?:\@|http?\://)\S+", "", doc)
                 for doc in documents]

    # Tokenize
    tokenizer = RegexpTokenizer(r'\w+')
    documents = [tokenizer.tokenize(doc.lower()) for doc in documents]

    unigrams = [w for doc in documents for w in doc if len(w) == 1]
    bigrams = [w for doc in documents for w in doc if len(w) == 2]

    stoplist = set(nltk.corpus.stopwords.words("english") + stoplist_tw + unigrams + bigrams)

    documents = [[token for token in doc if token not in stoplist]
                 for doc in documents]

    # Remove the numbers of the tweets
    documents = [[token for token in doc if len(token.strip(digits)) == len(token)]
                 for doc in documents]

    # Remove words that only occur once
    token_frequency = defaultdict(int)

    # Count all tokens
    for doc in documents:
        for token in doc:
            token_frequency[token] += 1

    # Keep only the words that occur more than once
    documents = [[token for token in doc if token_frequency[token] > 1]
                 for doc in documents]

    # Sort words in documents
    for doc in documents:
        doc.sort()

    # Build a dictionary where for each document each word has its own id
    dictionary = corpora.Dictionary(documents)
    dictionary.compactify()
    # and save the dictionary for future use
    dictionary.save('dictionary.dict')

    # We now have a dictionary with 26652 unique tokens
    print(dictionary)

    # Build the corpus: vectors with occurence of each word for each document
    # convert tokenized documents to vectors
    corpus = [dictionary.doc2bow(doc) for doc in documents]

    # and save in Market Matrix format
    corpora.MmCorpus.serialize('corpus.mm', corpus)

    # Load the corpus and Dictionary
    corpus = corpora.MmCorpus('corpus.mm')
    dictionary = corpora.Dictionary.load('dictionary.dict')

    print("LDA process begins now, please be patient, this will take a while...")

    lda = models.LdaModel(corpus, id2word=dictionary,
                          num_topics=number_of_topics,
                          passes=passes,
                          alpha=0.0001)

    results.write("\n" + keyword + ":\n\n")
    results.write(str(lda.print_topics()) + "\n")
    print("Topics saved\n")


#######################################################################
#
#   Main Program Begins Here
#
#######################################################################


print("\nLDA:\n")
print("This script runs LDA for each json file in the directory.")
print("Script starting:\n")

# Start timer
start = datetime.datetime.now()

# 23andMe
lda_analysis(path_of_file, 'name', 10, 100)

print("Finished the analysis")

# End timer
end = datetime.datetime.now()
print("Total time: " + str(end - start))
