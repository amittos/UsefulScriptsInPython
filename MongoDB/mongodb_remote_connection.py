#####################################################
#
#   Name: Alexandros Mittos
#   Institute: University College London
#   Date: May 2017
#   Language: Python 3
#
#   Information:    A sample script to connect to
#                   MongoDB remotely
#
#####################################################

#############################
#        Imports            #
#############################

import datetime
from sshtunnel import SSHTunnelForwarder
import pymongo
import pprint

#############################
#        Variables          #
#############################

# MongoDB Variables
MONGO_HOST = ""
MONGO_DB = ""
MONGO_USER = ""
MONGO_PASS = ""

#############################
#        Functions          #
#############################

# Awesome function No.1
def connect_to_mongo():

    # Initiate server using SSHTunnel
    server = SSHTunnelForwarder(
        MONGO_HOST,
        ssh_username=MONGO_USER,
        ssh_password=MONGO_PASS,
        remote_bind_address=('127.0.0.1', 27017)
    )

    # Start Connection
    server.start()

    # server.local_bind_port is assigned local port
    client = pymongo.MongoClient('127.0.0.1', server.local_bind_port)
    # Get the desired DB
    db = client[MONGO_DB]
    # Print the collections
    pprint.pprint(db.collection_names())

    # Stop Connection
    server.stop()

#############################
#           Main            #
#############################

if __name__ == "__main__":

    print('Process started...\n')

    # Start timer
    start = datetime.datetime.now()

    # Run awesome code
    connect_to_mongo()

    # End timer
    end = datetime.datetime.now()

    # Print results
    print("\nProcess finished")
    print("Total time: " + str(end - start))

#############################
#       End of Script       #
#############################