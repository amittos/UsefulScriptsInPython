#####################################################
#
#   Name: Alexandros Mittos
#   Institute: University College London
#   Date:
#   Language:
#
#   Information:
#
#
#
#
#####################################################

#############################
#        Imports            #
#############################

import datetime

#############################
#        Variables          #
#############################

path = '/home/aventinus/Dropbox/Development/'

#############################
#          Classes          #
#############################

class Example:

    # A class variable:
    department = 'School of Information'

    # A method:
    def set_name(self, new_name):
        self.name = new_name

    def set_location(self, new_location):
        self.location = new_location

#############################
#        Functions          #
#############################

# Awesome function No.1
def super_function(filename):
    # Run awesome function
    print(filename)

#############################
#           Main            #
#############################

if __name__ == "__main__":

    print('Process started...\n')

    # Start timer
    start = datetime.datetime.now()

    # Run awesome code
    super_function('Hey man!')

    # End timer
    end = datetime.datetime.now()

    # Print results
    print("\nProcess finished")
    print("Total time: " + str(end - start))

#############################
#       End of Script       #
#############################